package fr.upem.decode.message;

import java.nio.ByteBuffer;

public class MessageReader implements Reader<Message> {

    private final StringReader sr = new StringReader();

    private State state = State.WAITING_LOGIN;
    private Message message;
    private String login;
    private String text;

    private enum State {DONE,WAITING_LOGIN, WAITING_MESSAGE, ERROR};

    @Override
    public ProcessStatus process(ByteBuffer bb) {
        switch (state){
            case WAITING_LOGIN :
                var loginStatus = sr.process(bb);
                if(loginStatus != ProcessStatus.DONE) return loginStatus;
                login = sr.get();
                sr.reset();
                state = State.WAITING_MESSAGE;
            case WAITING_MESSAGE:
                var textStatus = sr.process(bb);
                if(textStatus != ProcessStatus.DONE) return textStatus;
                text = sr.get();
                sr.reset();
                message = new Message(login, text);
                state = State.DONE;
                return ProcessStatus.DONE;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public Message get() {
        if(state != State.DONE) throw new IllegalStateException();
        return message;
    }

    @Override
    public void reset() {
        login = null;
        state = State.WAITING_LOGIN;
    }
}
