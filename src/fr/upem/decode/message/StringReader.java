package fr.upem.decode.message;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;



public class StringReader implements Reader<String> {
    static private int BUFFER_SIZE = 1_024;
    private static final Charset UTF8 = StandardCharsets.UTF_8;
    private final IntReader intread = new IntReader();
    private ByteBuffer internalbb ; // write-mode
    private enum State {DONE,WAITING_INT,WAITING_TEXT,ERROR};
    private State state = State.WAITING_INT;
    private int size;
    private String text ;

    @Override
    public ProcessStatus process(ByteBuffer bb) {
        if (state== State.DONE || state== State.ERROR) {
            throw new IllegalStateException();
        }
        switch (state){
            case WAITING_INT:
                var sizeStatus = intread.process(bb);
                if(sizeStatus != ProcessStatus.DONE) return sizeStatus;
                size = intread.get();

                if(size > BUFFER_SIZE || size <= 0) return  ProcessStatus.ERROR;
                intread.reset();
                internalbb = ByteBuffer.allocate(size);
                state = State.WAITING_TEXT;

            case WAITING_TEXT:
                bb.flip();
                try{
                    if(bb.remaining() <= internalbb.remaining()){
                        internalbb.put(bb);
                    }else{
                        var oldLimit = bb.limit();
                        bb.limit(internalbb.remaining());
                        internalbb.put(bb);
                        bb.limit(oldLimit);
                    }
                }finally {
                    bb.compact();
                }
                if(internalbb.hasRemaining()) return ProcessStatus.REFILL;
                state = State.DONE;
                internalbb.flip();
                text = UTF8.decode(internalbb).toString();
                return ProcessStatus.DONE;

            default:
                throw new IllegalStateException();

        }

    }

    @Override
    public String get() {
        if(state != State.DONE) throw new IllegalStateException();
        return text;
    }

    @Override
    public void reset() {
        text = null;
        state = State.WAITING_INT;
    }

}
