package fr.upem.decode.message;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class Message {
    private final String login;
    private final String texte;

    public Message(String login, String texte) {
        this.login = login;
        this.texte = texte;
    }
}
