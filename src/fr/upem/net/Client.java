package fr.upem.net;

import fr.upem.decode.message.IntReader;
import fr.upem.decode.message.MessageReader;
import fr.upem.decode.message.Reader;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;

public class Client {
    static private class Context {
        final private SelectionKey key;
        final private SocketChannel sc;
        final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
        final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
        final private Queue<ByteBuffer> queue = new LinkedList<>(); // buffers read-mode
        final private MessageReader messageReader = new MessageReader();
        final private IntReader intReader = new IntReader();
        private boolean closed = false;
        private boolean connect = false; // pour l'identification
        private boolean command = false;
        /* LOGIN(0)
        /  MESSAGE(3)
        /  MESSAGE_PRIVATE(4)
        /  REQUEST_PRIVATE(5)
        /  ID_PRIVATE(8)
        /  LOGIN_PRIVATE(9)
        /  ESTABLISHED(10)
         */
        private Context(SelectionKey key) {
            this.key = key;
            this.sc = (SocketChannel) key.channel();
        }
        private void processIn() {
            for(;;){
                Reader.ProcessStatus status = intReader.process(bbin);
                switch (status){
                    case DONE:
                        Integer value = intReader.get();
                        processCode(value);
                        intReader.reset();
                        break;
                    case REFILL:
                        return;
                    case ERROR:
                        silentlyClose();
                        return;
                }
            }
        }
        private void processCode(int code){
            switch (code){
                case 1 :
                    connect = true;
                    System.out.println("Vous êtes connecté");
                case 2 :
                    System.out.println("La connection a eté refuse");
                    break;
                default:
                    //
                    break;
            }

        }

        /**
         * Add a message to the message queue, tries to fill bbOut and updateInterestOps
         *
         * @param bb
         */
        private void queueMessage(ByteBuffer bb) {
            queue.add(bb);
            processOut();
            updateInterestOps();
        }

        /**
         * Try to fill bbout from the message queue
         *
         */
        private void processOut() {
            while (!queue.isEmpty()){
                var bb = queue.peek();
                if (bb.remaining()<=bbout.remaining()){
                    queue.remove();
                    bbout.put(bb);
                } else {
                    break;
                }
            }
        }

        /**
         * Update the interestOps of the key looking
         * only at values of the boolean closed and
         * of both ByteBuffers.
         *
         * The convention is that both buffers are in write-mode before the call
         * to updateInterestOps and after the call.
         * Also it is assumed that process has been be called just
         * before updateInterestOps.
         */

        private void updateInterestOps() {
            var interesOps=0;
            if (!closed && bbin.hasRemaining()){
                interesOps=interesOps|SelectionKey.OP_READ;
            }
            if (bbout.position()!=0){
                interesOps|=SelectionKey.OP_WRITE;
            }
            if (interesOps==0){
                silentlyClose();
                return;
            }
            key.interestOps(interesOps);
        }

        private void silentlyClose() {
            try {
                sc.close();
            } catch (IOException e) {
                // ignore exception
            }
        }

        /**
         * Performs the read action on sc
         *
         * The convention is that both buffers are in write-mode before the call
         * to doRead and after the call
         *
         * @throws IOException
         */
        private void doRead() throws IOException {
            if (sc.read(bbin)==-1) {
                closed=true;
            }
            processIn();
            updateInterestOps();
        }

        /**
         * Performs the write action on sc
         *
         * The convention is that both buffers are in write-mode before the call
         * to doWrite and after the call
         *
         * @throws IOException
         */

        private void doWrite() throws IOException {
            bbout.flip();
            sc.write(bbout);
            bbout.compact();
            processOut();
            updateInterestOps();
        }

        public void doConnect() throws IOException {
            if(!sc.finishConnect()) return;
            key.interestOps(SelectionKey.OP_READ);
        }
    }
    static private int BUFFER_SIZE = 10_000; // a definir encore
    static private Logger logger = Logger.getLogger(Client.class.getName());

    private final String login;
    private final SocketChannel sc;
    private final Selector selector;
    private final InetSocketAddress serverAddress;
    private final Thread console;
    private final Charset UTF_8 = StandardCharsets.UTF_8;
    private final ArrayBlockingQueue<String> commandQueue = new ArrayBlockingQueue<>(10);
    private Context uniqueContext;

    public Client(String login, InetSocketAddress serverAddress) throws IOException {
        this.login = login;
        this.serverAddress = serverAddress;
        this.sc = SocketChannel.open();
        this.selector = Selector.open();
        this.console = new Thread(this::consoleRun);
    }
    private void consoleRun() {
        try {
            var scan = new Scanner(System.in);
            while (scan.hasNextLine()) {
                if(!uniqueContext.connect){
                    System.out.println("vous n'êtes pas identifier, taper 0 pour vous identifier ");
                }

                var command = scan.nextInt();
                switch (command){
                    case 0 :
                        askConnect();
                        break;
                    case 3 :
                        var msg = scan.nextLine();
                        sendMsgBroadcast(msg);
                        break;
                }

            }
        } catch (InterruptedException e) {
            logger.info("Console thread has been interrupted");
        } finally {
            logger.info("Console thread stopping");
        }
    }

    private void askConnect(){
        if(uniqueContext.closed ){
            Thread.currentThread().interrupt();
        }
        if(uniqueContext.connect){
            System.out.println("vous êtes déjà connecté !");
        }else
            selector.wakeup();

    }
    /**
     * Send a given command by the client with a given instruction
     * CONNECT -> ask a connection to the server
     * PRIVATE MESSAGE*/
    private void sendMsgBroadcast(String msg) throws InterruptedException {
        if(uniqueContext.closed || msg.length() > 1024) {
            Thread.currentThread().interrupt();
        }
        commandQueue.add(msg);
        selector.wakeup();
    }
    private void processCommands(){
        ByteBuffer sender = ByteBuffer.allocate(1024);
        if(commandQueue.isEmpty()) return;
        if(!uniqueContext.connect){
            sender.putInt(0);
            sender.putInt(login.length());
            sender.flip();
            this.uniqueContext.queueMessage(sender);
            sender.clear();
            return;
        }

        if(uniqueContext.closed) Thread.currentThread().interrupt();
        while(!commandQueue.isEmpty()) {
            String msg = commandQueue.poll();
            sender.putInt(3); // code pour le message broadcast
            sender.putInt(login.length());
            sender.put(UTF_8.encode(login));
            sender.putInt(msg.length());
            sender.put(UTF_8.encode(msg));

            sender.flip();
            this.uniqueContext.queueMessage(sender);
            sender.clear();
        }
    }
    public void launch() throws IOException {
        sc.configureBlocking(false);
        var key = sc.register(selector, SelectionKey.OP_CONNECT);
        uniqueContext = new Context(key);
        key.attach(uniqueContext);
        sc.connect(serverAddress);

        console.start();

        while(!Thread.interrupted()) {
            try {
                selector.select(this::treatKey);
                processCommands();
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            }
        }
    }
    private void treatKey(SelectionKey key) {
        try {
            if (key.isValid() && key.isConnectable()) {
                uniqueContext.doConnect();
            }
            if (key.isValid() && key.isWritable()) {
                uniqueContext.doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                uniqueContext.doRead();
            }
        } catch(IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
    }
    public static void main(String[] args) throws NumberFormatException, IOException {
        if (args.length!=3){
            usage();
            return;
        }
        new Client(args[0],new InetSocketAddress(args[1],Integer.parseInt(args[2]))).launch();
    }

    private static void usage(){
        System.out.println("Usage : ClientChat login hostname port");
    }
}
