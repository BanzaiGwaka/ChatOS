package fr.upem.net;

import fr.upem.decode.message.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.logging.Logger;

/* LOGIN(0)
/  LOGIN_ACCEPTED(1)
/  LOGIN_REFUSED(2)
/  MESSAGE(3)
/  MESSAGE_PRIVATE(4)
/  REQUEST_PRIVATE(5)
/  OK_PRIVATE(6)
/  KO_PRIVATE(7)
/  ID_PRIVATE(8)
/  LOGIN_PRIVATE(9)
/  ESTABLISHED(10)
 */
public class Chaton {
    static private class Context {
        final private SelectionKey key;
        final private SocketChannel sc;
        final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
        final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
        final private MessageReader messageReader = new MessageReader();
        final private IntReader intReader = new IntReader();
        final  private StringReader sr = new StringReader();
        final private Chaton server;
        final private List<Client> clients = new ArrayList<>();
        final private Queue<ByteBuffer> queue = new LinkedList<>();
        private boolean closed;

        private Context(Chaton server, SelectionKey key) {
            this.key = key;
            this.sc = (SocketChannel) key.channel();
            this.server = server;
        }

        private void processIn() {
            for(;;){
                Reader.ProcessStatus status = intReader.process(bbin);
                switch (status){
                    case DONE:
                        Integer value = intReader.get();
                        intReader.reset();
                        processCode(value);

                        break;
                    case REFILL:
                        return;
                    case ERROR:
                        silentlyClose();
                        return;
                }
            }
        }
        private void processCode(int code){
            ByteBuffer msg = ByteBuffer.allocate(BUFFER_SIZE);
            switch (code){
                case 0 :
                    for(;;){
                        Reader.ProcessStatus status = sr.process(bbin);
                        switch (status){
                            case DONE:
                                var value = sr.get();
                                if(clients.contains(value)) msg.putInt(2);
                                else msg.putInt(1);
                                msg.flip();
                                queueMessage(msg);
                                sr.reset();
                                break;
                            case REFILL:
                                return;
                            case ERROR:
                                silentlyClose();
                                return;
                        }
                        sr.reset();
                    }
                case 3 :
                    break;
                default:
                    //
                    break;
            }
        }
        private void queueMessage(ByteBuffer bb) {
            queue.add(bb);
            processOut();
            updateInterestOps();
        }
        /**
         * Try to fill bbout from the message queue
         *
         */
        private void processOut() {
            while (!queue.isEmpty()){
                var bb = queue.peek();
                if (bb.remaining()<=bbout.remaining()){
                    queue.remove();
                    bbout.put(bb);
                } else {
                    break;
                }
            }
        }

        private Message messageDecode(ByteBuffer bb){
            var status= messageReader.process(bbin);
            Message msg;
            for(;;) {
                switch (status) {
                    case DONE:
                        msg = messageReader.get();
                        messageReader.reset();
                        break;
                    case REFILL:

                    case ERROR:
                }
            }
        }
        private void updateInterestOps() {
            int newInterestOps = 0;
            if (bbin.hasRemaining() && !closed) {
                newInterestOps |= SelectionKey.OP_READ;
            }
            if(bbout.position() != 0) {
                newInterestOps |= SelectionKey.OP_WRITE;
            }
            if(newInterestOps ==0) {
                silentlyClose();
            } else {
                key.interestOps(newInterestOps);
            }
        }

        private void silentlyClose() {
            try {
                sc.close();
            } catch (IOException e) {
                // ignore exception
            }
        }

        /**
         * Performs the read action on sc
         *
         * The convention is that both buffers are in write-mode before the call
         * to doRead and after the call
         *
         * @throws IOException
         */
        private void doRead() throws IOException {
            if(sc.read(bbin) == -1 ) {
                closed = true;
            }
            processIn();

            updateInterestOps();
        }

        /**
         * Performs the write action on sc
         *
         * The convention is that both buffers are in write-mode before the call
         * to doWrite and after the call
         *
         * @throws IOException
         */

        private void doWrite() throws IOException {
            bbout.flip();
            sc.write(bbout);
            bbout.compact();
            updateInterestOps();
        }


    }

    static private final int BUFFER_SIZE = 10_000;
    static private Logger logger = Logger.getLogger(Chaton.class.getName());
    private final ArrayList<Context> contexts = new ArrayList<>();
    private final ServerSocketChannel serverSocketChannel;
    private final Selector selector;

    public Chaton(int port) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(port));
        selector = Selector.open();
        System.out.println("Le serveur a été demarré sur le port : " + port);
    }

    public void launch() throws IOException {
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        while(!Thread.interrupted()) {
            printKeys(); // for debug
            System.out.println("Starting select");
            try {
                selector.select(this::treatKey);
            } catch (UncheckedIOException tunneled) {
                throw tunneled.getCause();
            }
            System.out.println("Select finished");
        }
    }
    private void treatKey(SelectionKey key) {

        try {
            if (key.isValid() && key.isAcceptable()) {
                doAccept(key);
            }
        } catch(IOException ioe) {
            // lambda call in select requires to tunnel IOException
            throw new UncheckedIOException(ioe);
        }
        try {
            if (key.isValid() && key.isWritable()) {
                ((Context) key.attachment()).doWrite();
            }
            if (key.isValid() && key.isReadable()) {
                ((Context) key.attachment()).doRead();
            }
        } catch (IOException e) {

            silentlyClose(key);
        }
    }
    private void silentlyClose(SelectionKey key)  {
        Channel sc = (Channel) key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }
    private void doAccept(SelectionKey key) throws IOException {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel sc = ssc.accept();
        if ( sc == null) {
            return; // the selector gave a bad hint
        }
        sc.configureBlocking(false);
        var keyClient = sc.register(selector,SelectionKey.OP_READ);
        var context = new Context(this, keyClient);
        contexts.add(context);
        keyClient.attach(context);
    }

    public static void main(String[] args) throws IOException{
        if(args.length!=1) {
            usage();
            return;
        }
        new Chaton(Integer.parseInt(args[0])).launch();
    }
    private String interestOpsToString(SelectionKey key){
        if (!key.isValid()) {
            return "CANCELLED";
        }
        int interestOps = key.interestOps();
        ArrayList<String> list = new ArrayList<>();
        if ((interestOps&SelectionKey.OP_ACCEPT)!=0) list.add("OP_ACCEPT");
        if ((interestOps&SelectionKey.OP_READ)!=0) list.add("OP_READ");
        if ((interestOps&SelectionKey.OP_WRITE)!=0) list.add("OP_WRITE");
        return String.join("|",list);
    }
    private static void usage(){
        System.out.println("Usage : Chaton port");
    }
    public void printKeys() {
        Set<SelectionKey> selectionKeySet = selector.keys();
        if (selectionKeySet.isEmpty()) {
            System.out.println("The selector contains no key : this should not happen!");
            return;
        }
        System.out.println("The selector contains:");
        for (SelectionKey key : selectionKeySet){
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tKey for ServerSocketChannel : "+ interestOpsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tKey for Client "+ remoteAddressToString(sc) +" : "+ interestOpsToString(key));
            }
        }
    }
    private String remoteAddressToString(SocketChannel sc) {
        try {
            return sc.getRemoteAddress().toString();
        } catch (IOException e){
            return "???";
        }
    }
}
